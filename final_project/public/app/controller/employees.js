
app.controller('myCtrl', function($scope, $http){
	 		


		$http({
		  method: 'GET',
		  url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees'
		}).then(function successCallback(response) {
		     $scope.informations = response.data;
		     // $('#modalConnect').modal('hide');
		}, function myError(response) {
        	$('#modalConnect').modal('show');
    	});

	$scope.addEmploy = function(){

		$http({
		  method: 'POST',
		  url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees',
		  data: {"name" : $scope.yourName,"email" : $scope.email,"contact_number" : $scope.phone,"position" : $scope.yourPosition}
		}).then(function successCallback(response) {
		     $scope.informations = response.data;
		     location.reload();
		},function myError(response) {
			$('#myModal').modal('hide');
        	$('#modalCreateError').modal('show');

    	});
		
	};

	
		// delete
	$scope.getIdDel = function(){
		$('#modalDelete').modal('show');
	};	

	$scope.deleteEmployees = function(id){
		$http({
			  method: 'DELETE',
			  url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees/' + id
			}).then(function successCallback(response) {
			     $scope.informations = response.data;
			     // $scope.getId = id;
				
			});
			location.reload();
	};	
	
	
	$scope.editData = function(id){
	
		$http({
			  method: 'GET',
			  url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees/' + id
			}).then(function successCallback(response) {
			    $scope.informationDetails = response.data;		
			});

		$('#myModalEdit').modal('show');
	};

	$scope.saveEdit = function(id){
		$http({
			  method: 'POST',
			  url: 'http://192.168.1.31:8080/angulara/public/api/v1/employees/' + id,
			  data: $scope.informationDetails
			}).then(function successCallback() {	     
				location.reload();
			}, function myError(response) {
				$('#myModalEdit').modal('hide');
        		$('#modalUpdateError').modal('show');
    		});
			
		};

	// show detail
	$scope.showDetail = function(id){
		
		$http({
			  method: 'GET',
			  url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees/' + id
			}).then(function successCallback(response) {
			    $scope.informationDetails = response.data;    
				
			});	
	};


	// pagination

	

    $http({
		  method: 'GET',
		  url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees_count'
		}).then(function successCallback(response) {
		     $scope.totalSize = response.data;
		     // console.log($scope.totalSize);
		     var i = 1;
		    $scope.pagingHtml = '';
		    $scope.totalPage = Math.ceil($scope.totalSize/5);
		    if(i == 1 && $scope.totalPage<=8){
			    while(i < $scope.totalPage + 1){
			    	if(i == $scope.pageActive){
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link active' href='' ng-click='activePageData("+i+")'>"+ i +"</a></li>";
			    	}else{
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+i+")'>"+ i +"</a></li>";
				    	
			    	}    	
			    	i++;
			    }
			}
				// if(i == 1 && $scope.totalPage<=8){
				//     $scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link active' href='' ng-click='activePageData("+i+")'>"+ i +"</a></li>";
		  //   		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(i+1)+")'>"+ (i+1) +"</a></li>";
		  //   		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(i+2)+")'>"+ (i+2) +"</a></li>";
		  //   		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(i+3)+")'>"+ (i+3) +"</a></li>";
		  //   		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(i+4)+")'>"+ (i+4) +"</a></li>";
		  //   		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(i+5)+")'>"+ (i+5) +"</a></li>";
		  //   		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(i+6)+")'>"+ (i+6) +"</a></li>";
		  //   		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+($scope.totalPage)+")'>"+ $scope.totalPage +"</a></li>";
				// }
				else if(i == 1 && $scope.totalPage>8){		
		    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link active' href='' ng-click='activePageData("+i+")'>"+ i +"</a></li>";
		    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(i+1)+")'>"+ (i+1) +"</a></li>";
		    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(i+2)+")'>"+ (i+2) +"</a></li>";
		    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(i+3)+")'>"+ (i+3) +"</a></li>";
		    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(i+4)+")'>"+ (i+4) +"</a></li>";
		    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(i+5)+")'>"+ (i+5) +"</a></li>";
		    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(i+6)+")'>"+ (i+6) +"</a></li>";
			    	$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href=''>...</a></li>";
		    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+($scope.totalPage)+")'>"+ $scope.totalPage +"</a></li>";
				    }    	
				    	
			

		});

		
	$scope.pageActive = 1;
	$scope.activePageData = function(p){
		$scope.pageActive = p;
    	$http({
		  method: 'GET',
		  url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees?page=' + p
		}).then(function successCallback(response) {
		     $scope.informations = response.data;

		    var i = 1;
			var currentPage = p;
			$scope.pagingHtml = '';
			$scope.totalPage = Math.ceil($scope.totalSize/5);
				if (currentPage<6) {
				    while(i < $scope.totalPage + 1){
				    	if(i == currentPage){
				    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link active' href='' ng-click='activePageData("+currentPage+")'>"+ currentPage +"</a></li>";
				    	}
				    	else{
				    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+i+")'>"+ i +"</a></li>";  	
				    	}    	
				    	i++;
				    }
				}
			    else if(currentPage>=6 && $scope.totalPage>=8 && currentPage<=($scope.totalPage-2)){
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href=''>...</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(currentPage-3)+")'>"+ (currentPage-3) +"</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(currentPage-2)+")'>"+ (currentPage-2) +"</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(currentPage-1)+")'>"+ (currentPage-1) +"</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link active' href='' ng-click='activePageData("+currentPage+")'>"+ currentPage +"</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(currentPage+1)+")'>"+ (currentPage+1) +"</a></li>";
			    		if((currentPage+1) < ($scope.totalPage-1)){
			    			$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href=''>...</a></li>";
			    		}
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+$scope.totalPage+")'>"+ $scope.totalPage +"</a></li>";
			    	}
			    	else if(currentPage == ($scope.totalPage-1)){
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href=''>...</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(currentPage-5)+")'>"+ (currentPage-5) +"</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(currentPage-4)+")'>"+ (currentPage-4) +"</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(currentPage-3)+")'>"+ (currentPage-3) +"</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(currentPage-2)+")'>"+ (currentPage-2) +"</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(currentPage-1)+")'>"+ (currentPage-1) +"</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link active' href='' ng-click='activePageData("+currentPage+")'>"+ currentPage +"</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+$scope.totalPage+")'>"+ $scope.totalPage +"</a></li>";
			    	}
			    	else if(currentPage == $scope.totalPage){
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href=''>...</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(currentPage-5)+")'>"+ (currentPage-5) +"</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(currentPage-4)+")'>"+ (currentPage-4) +"</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(currentPage-3)+")'>"+ (currentPage-3) +"</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(currentPage-2)+")'>"+ (currentPage-2) +"</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+(currentPage-1)+")'>"+ (currentPage-1) +"</a></li>";
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link active' href='' ng-click='activePageData("+currentPage+")'>"+ currentPage +"</a></li>";
			    	}   	
			    	// i++;

		 })
		

		
    }
    
    $scope.nextPageData = function(){
    	if($scope.pageActive < Math.ceil($scope.totalSize/5)){
    		$scope.pageActive++;
    	}
    	//alert(1);
    	 
    	$http({
		  method: 'GET',
		  url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees?page=' + $scope.pageActive
		}).then(function successCallback(response) {
		     $scope.informations = response.data;

		      var i = 1;
			$scope.pagingHtml = '';
			$scope.totalPage = Math.ceil($scope.totalSize/5);
			if($scope.pageActive<$scope.totalPage){
			    while(i < $scope.totalPage + 1){
			    	if(i == $scope.pageActive){
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link active' href='' ng-click='activePageData("+i+")'>"+ i +"</a></li>";
			    	}else{
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+i+")'>"+ i +"</a></li>";
				    	
			    	}    	
			    	i++;
			    }
			}
			else if($scope.pageActive==$scope.totalPage){
			    $scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href=''>...</a></li>";
			    $scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+($scope.pageActive-5)+")'>"+ ($scope.pageActive-5) +"</a></li>";
			    $scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+($scope.pageActive-4)+")'>"+ ($scope.pageActive-4) +"</a></li>";
			    $scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+($scope.pageActive-3)+")'>"+ ($scope.pageActive-3) +"</a></li>";
			    $scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+($scope.pageActive-2)+")'>"+ ($scope.pageActive-2) +"</a></li>";
			    $scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+($scope.pageActive-1)+")'>"+ ($scope.pageActive-1) +"</a></li>";
			    $scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link active' href='' ng-click='activePageData("+$scope.pageActive+")'>"+ $scope.pageActive +"</a></li>";
			} 
			
		})
    }

     $scope.prevPageData = function(){
    	if($scope.pageActive>1){
    		$scope.pageActive--;
    	}
    	//alert(1);
    	 
    	$http({
		  method: 'GET',
		  url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees?page=' + $scope.pageActive
		}).then(function successCallback(response) {
		     $scope.informations = response.data;

		      var i = 1;
			$scope.pagingHtml = '';
			    while(i < Math.ceil($scope.totalSize/5) + 1){
			    	if(i == $scope.pageActive){
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link active' href='' ng-click='activePageData("+i+")'>"+ i +"</a></li>";
			    	}else{
			    		$scope.pagingHtml += "<li class='page-item page-itemNum' style='display:inline-block' ><a class='page-link' href='' ng-click='activePageData("+i+")'>"+ i +"</a></li>";
				    	
			    	}    	
			    	i++;
			    }
			
		})
    }

    // pagination


});


app.directive("paginationNum", function($compile) {
    return {
        link: function($scope, element){
            $scope.$watch("pagingHtml",function(newValue,oldValue) {
                element.html($compile(newValue)($scope));
            });
        }
    }
});

	 // pagination





